﻿#include "GameScene.h"
#include "SimpleAudioEngine.h"
using namespace cocos2d;
using namespace CocosDenshion;

namespace
{
#include "GameConfig.inl"
    const float pi = 3.141592653589793f;

    Vec2 unitVector(float radian)
    {
        return Vec2(std::cosf(radian), std::sinf(radian));
    }

    void beginDraw(DrawNode* drawNode)
    {
        drawNode->clear();
    }

    void drawTriangle(DrawNode* drawNode, Vec2 scrPos, Vec2 dirVec, float radius, Color4F color)
    {
        dirVec.normalize();
        Vec2 dirPoint = scrPos + radius * dirVec;

        Vec2 perDir = Vec2(-dirVec.y, dirVec.x);   // 진행방향의 수직
        float perLen = std::sinf(pi / 6);
        float dirLen = std::cosf(pi / 6);
        Vec2 leftPoint = scrPos + radius * (perLen * perDir - dirLen * dirVec);
        Vec2 rightPoint = scrPos + radius * (-perLen * perDir - dirLen * dirVec);
        drawNode->drawTriangle(dirPoint, leftPoint, rightPoint, color);
    }

    void drawCircle(DrawNode* drawNode, Vec2 srcPos, float radius, Color4F color)
    {
        float circum = 2 * pi * radius;
        int vertCount = 10; // circum / 20;
        vertCount = std::max(vertCount, 20);
        std::unique_ptr<Vec2[]> verts = std::unique_ptr<Vec2[]>(new Vec2[vertCount]);
        for (int i = 0; i < vertCount; i++)
        {
            float rad = 2 * pi / vertCount * i;
            verts[i] = unitVector(rad) * radius + srcPos;
        }
        drawNode->drawPolygon(verts.get(), vertCount, Color4F(0, 0, 0, 0), 1, color);
    }

    void drawRect(DrawNode* drawNode, const Rect& area, float thickNess, Color4F fill, Color4F border)
    {
        Vec2 verts[4] = { area.origin, area.origin, area.origin, area.origin };
        verts[1].x += area.size.width;
        verts[2].x += area.size.width;
        verts[2].y += area.size.height;
        verts[3].y += area.size.height;
        drawNode->drawPolygon(verts, 4, fill, thickNess, border);
    }

    float massToRadius(float mass)
    {
        return std::sqrtf(mass) * 1.5f;
    }

    float randf()
    {
        return (rand() % 1000) / 1000.0f;
    }

    float randf(float min, float max)
    {
        return randf() * (max - min) + min;
    }

    void setSpriteSize(Sprite* sprite, const Size& size)
    {
        Size srcSize = sprite->getContentSize();
        sprite->setScale(size.width / srcSize.width, size.height / srcSize.height);
    }

    void playNonCopyrightEffect(const char* path)
    {
        if (nonCopyrightMode)
            SimpleAudioEngine::getInstance()->playEffect(path);
    }

    bool isTouchInNode(Node* node, Touch* touch)
    {
        auto pos = node->convertTouchToNodeSpace(touch);
        auto size = node->getContentSize();
        if (pos.x < 0) return false;
        if (pos.y < 0) return false;
        if (pos.x > size.width) return false;
        if (pos.y > size.height) return false;
        return true;
    }
}

class DotStarLayer : public Layer
{
    static const int subCount = 4;
    Node* subComponents[subCount];
public:

    bool init()
    {
        if (!Layer::init()) return false;

        Size scrSize = Director::getInstance()->getWinSize();

        for (int i = 0; i < subCount; i++)
        {
            if (nonCopyrightMode) subComponents[i] = SpriteBatchNode::create("non_copyright/heart.png");
            else subComponents[i] = DrawNode::create();
            
            const int starCountPerSub = 75;
            Vec3 baseColor1 = Vec3(1, 0.25f, 0);
            Vec3 baseColor2 = Vec3(1, 0.75f, 0);
            for (int j = 0; j < starCountPerSub; j++)
            {
                float factor = randf();
                Vec3 c = baseColor1 * factor + baseColor2 * (1 - factor);
                Color4F color = Color4F(c.x, c.y, c.z, 1);
                color.a = randf(0.5f, 1.0f);

                Vec2 pos = Vec2(rand() % (int)scrSize.width, rand() % (int)scrSize.height);

                if (nonCopyrightMode)
                {
                    auto parent = (SpriteBatchNode*)subComponents[i];
                    auto heart = Sprite::createWithTexture(parent->getTexture());
                    heart->setPosition(pos);
                    heart->setColor(Color3B(color));
                    heart->setOpacity((int)(255 * color.a));
                    heart->setBlendFunc(BlendFunc::ADDITIVE);
                    heart->setScale(randf(0.25f, 0.5f));
                    parent->addChild(heart);
                }
                else
                {
                    ((DrawNode*)subComponents[i])->drawDot(pos, 1, color);
                }
            }

            addChild(subComponents[i]);
        }

        setCamera(Vec2::ZERO);

        return true;
    }

    static DotStarLayer* create()
    {
        auto ret = new DotStarLayer();
        if (ret && ret->init())
            ret->autorelease();
        else
            CC_SAFE_RELEASE_NULL(ret);
        return ret;
    }

    void setCamera(Vec2 pos)
    {
        Size scrSize = Director::getInstance()->getWinSize();
        int w = (int)scrSize.width;
        int h = (int)scrSize.height;
        int x = (int)-pos.x;
        int y = (int)-pos.y;
        
        int xMod = x % (2 * w) - w;
        int yMod = y % (2 * h) - h;
        if (xMod < -w) xMod += 2 * w;
        if (yMod < -h) yMod += 2 * h;
        int xAlt = (xMod < 0) ? xMod + w : xMod - w;
        int yAlt = (yMod < 0) ? yMod + h : yMod - h;
        subComponents[0]->setPosition(Vec2(xMod, yMod));
        subComponents[1]->setPosition(Vec2(xAlt, yMod));
        subComponents[2]->setPosition(Vec2(xMod, yAlt));
        subComponents[3]->setPosition(Vec2(xAlt, yAlt));
    }
};

class GameLayer : public Layer
{
    const float pixelToKm = 30;
    const float shipRadius = 20;
    const float startVelocity = 400;
    const float maxEnergy = 5.0f;
    const float energyRegen = 1.0f;
    const float energyConsume = 3.0f;
    const int maxBombCharge = 3;
    const int bombPerDisplacement = (int)(50 * pixelToKm);
    const float boostChargeSpeed = 27 * pixelToKm;

    float maxDisplacement = 0;
    float displacement;
    Vec2 position;
    Vec2 velocity;
    Vec2 direction;
    float startAngle;
    float boostRemains;
    float energy = 0;
    int bombCharge = 0;
    bool boostCharge = false;

    bool isOpening = true;
    bool isLaunched = false;
    bool isInAcceleration = false;
    bool isBoosting = false;
    bool isDead = false;
    bool isEnding = false;

    Action* restartAction;
    Action* playBGMAction = nullptr;

    Sprite* ship;
    SpriteBatchNode* massObjectSprites;

    DrawNode* energyBar;
    DrawNode* drawNode;
    Label* speedLabel;
    Label* distanceLabel;

    Sprite* bombIcon;
    Sprite* boostIcon;
    Label* bombChargeLabel;

    Sprite* title = nullptr;
    Sprite* objective = nullptr;
    Sprite* startTip = nullptr;
    Sprite* skillReady = nullptr;
    LayerColor* endDark = nullptr;
    Sprite* endText = nullptr;
    Sprite* resultText = nullptr;
    Label* resultLabel = nullptr;
    Sprite* bestText = nullptr;
    Label* bestLabel = nullptr;

    static const int bgCount = 5;
    DotStarLayer* starBg[bgCount];

    struct MassObject
    {
        MassObject(const Vec2& pos, float mass, Sprite* sprite)
            : pos(pos)
            , mass(mass)
            , sprite(sprite)
        {
        }

        Vec2 pos;
        float mass;
        Sprite* sprite;
    };
    std::vector<MassObject> massObjects;

public:
    bool init()
    {
        if (!Layer::init()) return false;

        Size scrSize = Director::getInstance()->getWinSize();

        if (nonCopyrightMode)
        {
            do {
                ship = Sprite::create("non_copyright/ship.png");
                if (ship == nullptr)
                {
                    nonCopyrightMode = false;
                    break;
                }
                setSpriteSize(ship, Size(shipRadius * 2, shipRadius * 2));
                ship->setAnchorPoint(Vec2(0.5f, 0.5f));
                addChild(ship);

                massObjectSprites = SpriteBatchNode::create("non_copyright/massObject.png");
                addChild(massObjectSprites);
            } while (0);
        }

        for (int i = 0; i < bgCount; i++)
        {
            addChild(starBg[i] = DotStarLayer::create(), -1);
        }

        drawNode = DrawNode::create();
        addChild(drawNode);

        energyBar = DrawNode::create();
        addChild(energyBar);

        speedLabel = Label::createWithBMFont("fonts/konqa32.fnt", "123 km/s");
        speedLabel->setColor(Color3B(64, 255, 128));
        speedLabel->setAnchorPoint(Vec2(1, 1));
        speedLabel->setPosition(Vec2(scrSize.width - 10, scrSize.height - 10));
        speedLabel->setScale(1.5f);
        addChild(speedLabel);

        distanceLabel = Label::createWithBMFont("fonts/konqa32.fnt", "123 km");
        distanceLabel->setColor(Color3B(64, 255, 128));
        distanceLabel->setAnchorPoint(Vec2(1, 1));
        distanceLabel->setPosition(Vec2(scrSize.width - 10, scrSize.height - 65));
        addChild(distanceLabel);

        bombIcon = Sprite::create("icons/Bomb.png");
        bombIcon->setPosition(Vec2(20, 20));
        bombIcon->setAnchorPoint(Vec2::ZERO);
        bombIcon->setColor(Color3B::GRAY);
        bombIcon->setOpacity(64);
        addChild(bombIcon, 1);

        bombChargeLabel = Label::createWithBMFont("fonts/konqa32.fnt", "0");
        bombChargeLabel->setColor(Color3B(64, 0, 0));
        bombChargeLabel->setAnchorPoint(Vec2(0.5f, 0.5f));
        bombChargeLabel->setPosition(Vec2(20, 20) + Vec2(128, 128) / 2);
        bombChargeLabel->setScale(1.75f);
        addChild(bombChargeLabel, 2);

        boostIcon = Sprite::create("icons/Boost.png");
        boostIcon->setPosition(Vec2(128 + 20 + 20, 20));
        boostIcon->setAnchorPoint(Vec2(0, 0));
        boostIcon->setColor(Color3B::GRAY);
        boostIcon->setOpacity(64);
        addChild(boostIcon, 1);

        if (nonCopyrightMode)
        {
            endDark = LayerColor::create(Color4B(0, 0, 0, 128));
            addChild(endDark);

            isOpening = true;
            title = Sprite::create("non_copyright/title.png");
            title->setAnchorPoint(Vec2(0.5f, 0.5f));
            title->setPosition(Vec2(scrSize.width / 2, scrSize.height * 0.75f));
            title->setOpacity(0);
            title->runAction(FadeIn::create(1.25f));
            addChild(title);

            objective = Sprite::create("non_copyright/objective.png");
            objective->setAnchorPoint(Vec2(0.5f, 0.5f));
            objective->setPosition(Vec2(scrSize.width / 2, scrSize.height * 0.33f));
            objective->setOpacity(0);
            objective->runAction(Sequence::create(
                DelayTime::create(0.5f),
                FadeIn::create(5.0f),
                nullptr));
            addChild(objective);

            float top = scrSize.height * 0.75f;
            endText = Sprite::create("non_copyright/endText.png");
            endText->setAnchorPoint(Vec2(0.5f, 1));
            endText->setPosition(Vec2(scrSize.width / 2, top));
            endText->setVisible(false);
            addChild(endText);

            top -= endText->getContentSize().height * 2;
            resultText = Sprite::create("non_copyright/result.png");
            resultText->setAnchorPoint(Vec2(1, 1));
            resultText->setPosition(Vec2(scrSize.width / 2, top));
            resultText->setVisible(false);
            addChild(resultText);

            resultLabel = Label::createWithTTF("123 $", "fonts/SeoulHangangM.ttf", 48);
            resultLabel->setAnchorPoint(Vec2(0, 1.25f));
            resultLabel->setPosition(Vec2(scrSize.width / 2, top));
            resultLabel->setVisible(false);
            addChild(resultLabel);

            top -= resultText->getContentSize().height;
            
            bestText = Sprite::create("non_copyright/best.png");
            bestText->setAnchorPoint(Vec2(1, 1));
            bestText->setPosition(Vec2(scrSize.width / 2, top));
            bestText->setVisible(false);
            addChild(bestText);

            bestLabel = Label::createWithTTF("123 $", "fonts/SeoulHangangM.ttf", 48);
            bestLabel->setAnchorPoint(Vec2(0, 1.25f));
            bestLabel->setPosition(Vec2(scrSize.width / 2, top));
            bestLabel->setVisible(false);
            addChild(bestLabel);
        }
        else
        {
            isOpening = false;
        }

        auto touchListener = EventListenerTouchOneByOne::create();
        touchListener->onTouchBegan = CC_CALLBACK_2(GameLayer::onTouchBegan, this);
        touchListener->onTouchMoved = CC_CALLBACK_2(GameLayer::onTouchMoved, this);
        touchListener->onTouchEnded = CC_CALLBACK_2(GameLayer::onTouchEnded, this);
        touchListener->onTouchCancelled = CC_CALLBACK_2(GameLayer::onTouchCancelled, this);
        getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);

        auto keyboardListener = EventListenerKeyboard::create();
        keyboardListener->onKeyPressed = CC_CALLBACK_2(GameLayer::onKeyPressed, this);
        getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);
        
        initNewSession();
        scheduleUpdate();

        return true;
    }

    void initNewSession()
    {
        // 로직 초기화
        startAngle = 0;
        displacement = 0;
        position = Vec2::ZERO;
        velocity = Vec2::ZERO;
        direction = Vec2(1, 0);
        energy = maxEnergy;

        isLaunched = false;
        isInAcceleration = false;
        isDead = false;
        restartAction = nullptr;
     
        bombCharge = 0;
        resetBombButton();

        boostCharge = false;
        resetBoostButton();

        massObjects.clear();
        if (nonCopyrightMode) massObjectSprites->removeAllChildrenWithCleanup(true);
        placeOrbit(Vec2::ZERO);
    }

    void endSession()
    {
        if (!nonCopyrightMode)
        {
            initNewSession();
            return;
        }

        endDark->setVisible(true);
        endText->setVisible(true);
        resultText->setVisible(true);
        resultLabel->setVisible(true);
        bestText->setVisible(true);
        bestLabel->setVisible(true);

        resultLabel->setString(distanceLabel->getString());
        if (maxDisplacement < displacement)
        {
            maxDisplacement = displacement;
            bestLabel->setString(distanceLabel->getString());
        }

        isEnding = true;
    }

    void resetBombButton()
    {
        bombIcon->setColor(Color3B::GRAY);
        bombIcon->setOpacity(64);
        bombIcon->stopAllActions();
        bombChargeLabel->setColor(Color3B(64, 0, 0));
        bombChargeLabel->setString(std::to_string(bombCharge));
    }

    void resetBoostButton()
    {
        boostIcon->setColor(Color3B::GRAY);
        boostIcon->setOpacity(64);
        boostIcon->stopAllActions();
    }

    void placeOrbit(Vec2 center)
    {
        addOrbit(center, 250, 550, 250, 560, 4);
        addOrbit(center, 600, 700, 250, 560, 3);
        addOrbit(center, 750, 1250, 250, 750, 10);
        addOrbit(center, 1500, 2800, 350, 1550, 20);
    }

    void addOrbit(Vec2 basePos, float minRadius, float maxRadius, float minMass, float maxMass, int count)
    {
        float angleSlice = 2 * pi / count;
        for (int i = 0; i < count; i++)
        {
            float radius = randf(minRadius, maxRadius);
            float mass = randf(minMass, maxMass);
            float angle = angleSlice * i + randf(0, angleSlice * 0.80f);
            auto pos = basePos + unitVector(angle) * radius;

            Sprite* sprite = nullptr;
            if (nonCopyrightMode)
            {
                int maxVariation = 9;
                int tx = 96 * (rand() % maxVariation);
                sprite = Sprite::createWithTexture(massObjectSprites->getTexture(), Rect(tx, 0, 95, 96));
                sprite->setPosition(pos);
                setSpriteSize(sprite, Size(massToRadius(mass) * 2, massToRadius(mass) * 2));
                massObjectSprites->addChild(sprite);
            }
            massObjects.push_back(MassObject(basePos + unitVector(angle) * radius, mass, sprite));
        }
    }

    void update(float dt)
    {
        Size scrSize = Director::getInstance()->getWinSize();
        Vec2 centerScr = Vec2(scrSize.width / 2, scrSize.height / 2);

        if (isInAcceleration)
        {
            energy -= dt * energyConsume;
            energy = std::max(0.0f, energy);
        }
        else if (!isDead)
        {
            energy += dt * energyRegen;
            energy = std::min(energy, maxEnergy);
        }

        // 게임 상태 플래그 결정
        bool gravityEnable = isLaunched && !isDead && !isBoosting;
        gravityEnable = gravityEnable && !(isInAcceleration && energy > 0);
        bool collisionEnable = !isDead && !isBoosting;

        // 추진력 합산
        if (isInAcceleration && energy > 0)
        {
            velocity += (velocity.getNormalized() * 40) * dt;
        }

        // 중력 계산
        bool allExcluded = true;
        for (const auto& obj : massObjects)
        {
            const float unitDistance = 50;
            const float gravityMultipler = 7.5f;

            Vec2 toObject = (obj.pos - position);
            float distToObject = toObject.length() / unitDistance;
            toObject.normalize();
            Vec2 gravityForce = toObject / (distToObject * distToObject) * obj.mass * gravityMultipler;
            if (gravityForce.length() < 2.0f) continue;
            if (gravityEnable) velocity += gravityForce * dt;
            allExcluded = false;
        }

        if (isBoosting)
        {
            boostRemains -= dt;
        }

        // 모든 중력 범위를 벗어난 경우, 행성 재배치
        if (allExcluded)
        {
            massObjects.clear();
            if (nonCopyrightMode) massObjectSprites->removeAllChildrenWithCleanup(true);
            placeOrbit(position + velocity.getNormalized() * 3500.0f);

            if (isBoosting && boostRemains < 0)
            {
                velocity = velocity.getNormalized() * startVelocity;
                isBoosting = false;
            }
        }

        if (collisionEnable)
        {
            // 연속 지점 충돌 여부 판정
            auto deltaPos = velocity * dt;
            auto deltaLen = deltaPos.length();
            auto normalizedVelocity = (velocity == Vec2::ZERO) ? Vec2::ZERO : velocity.getNormalized();
            for (const auto& obj : massObjects)
            {
                float sumOfRadius = shipRadius + massToRadius(obj.mass);

                if (obj.pos.getDistance(position) > deltaLen + sumOfRadius) continue;
                for (float testDelta = 0; testDelta < deltaLen; testDelta += shipRadius)
                {
                    auto posInContLine = position + normalizedVelocity * testDelta;
                    if (obj.pos.getDistance(posInContLine) < sumOfRadius)
                    {
                        isDead = true;
                        playNonCopyrightEffect("non_copyright/dead.wav");
                        if (playBGMAction != nullptr)
                        {
                            stopAction(playBGMAction);
                            playBGMAction = nullptr;
                        }
                        SimpleAudioEngine::getInstance()->stopBackgroundMusic();
                        position = posInContLine;
                        resetBombButton();
                        resetBoostButton();
                        runAction(restartAction = Sequence::create(
                            DelayTime::create(1.0f),
                            CallFunc::create(CC_CALLBACK_0(GameLayer::endSession, this)),
                            nullptr));
                        goto endCollision;
                    }
                }
            }

        endCollision:;
        }

        // 방향 업데이트 및 속도 업데이트
        if (!isDead)
        {
            if (!isLaunched)
            {
                startAngle += 0.25f * 2 * pi * dt;
                direction = unitVector(startAngle);
            }
            else if (velocity != Vec2::ZERO)
            {
                direction = velocity;
            }
            float oldDisplacement = displacement;
            displacement += (velocity * dt).length();
            if (bombCharge < maxBombCharge 
                && (((int)oldDisplacement) / bombPerDisplacement) != (((int)displacement) / bombPerDisplacement))
            {
                bombCharge++;
                bombChargeLabel->setString(std::to_string(bombCharge));
                if (bombCharge == 1) // 0에서 1이 된 경우
                {
                    bombChargeLabel->setColor(Color3B::WHITE);
                    bombIcon->setColor(Color3B(64, 255, 128));
                    bombIcon->runAction(RepeatForever::create(
                        Sequence::create(
                            FadeTo::create(0.5f, 255),
                            FadeTo::create(0.5f, 128),
                            nullptr)));
                }
            }
            if (!boostCharge && velocity.length() > boostChargeSpeed && !isBoosting)
            {
                boostCharge = true;
                boostIcon->setColor(Color3B(64, 255, 128));
                boostIcon->runAction(RepeatForever::create(
                    Sequence::create(
                    FadeTo::create(0.5f, 255),
                    FadeTo::create(0.5f, 128),
                    nullptr)));
            }

            position += velocity * dt;
        }
        else
        {
            velocity = Vec2::ZERO;
        }

        // UI 업데이트
        if (nonCopyrightMode)
        {
            const float dollarToKm = 0.5f;
            speedLabel->setString(std::to_string((int)(velocity.length() / pixelToKm * dollarToKm)) + " $/s");
            distanceLabel->setString(std::to_string((int)(displacement / pixelToKm * dollarToKm)) + " $");
        }
        else
        {
            speedLabel->setString(std::to_string((int)(velocity.length() / pixelToKm)) + " km/s");
            distanceLabel->setString(std::to_string((int)(displacement / pixelToKm)) + " km");
        }

        energyBar->clear();
        {
            int left = 20, bottom = 128 + 40;
            int width = 48, height = scrSize.height - bottom - 20;
            int p = 8;

            Color4F borderColor = Color4F(0.25f, 1, 0.5f, 1);
            Color4F fillColor = Color4F(0.25f, 1, 0.5f, 0.5f);
            if (isDead || !isLaunched || energy <= 0)
            {
                borderColor = Color4F(0.5f, 0.5f, 0.5f, 0.5f);
                fillColor = Color4F(0.25f, 0.25f, 0.25f, 0.25f);
            }
            float fillHeight = (height - p * 2) * energy / maxEnergy;
            drawRect(energyBar, Rect(left, bottom, width, height), 2, Color4F(0, 0, 0, 0), borderColor);
            drawRect(energyBar, Rect(left + p, bottom + p, width - 2 * p, fillHeight), 2, fillColor, Color4F(0, 0, 0, 0));
        }

        // 게임 렌더링 업데이트
        float bgDepth[bgCount] = { 2.0f, 2.15f, 2.33f, 2.5f, 3.0f };
        for (int i = 0; i < bgCount; i++)
        {
            starBg[i]->setCamera(position / bgDepth[i]);
        }

        if (nonCopyrightMode)
        {
            ship->setPosition(centerScr);
            ship->setRotation(std::atan2f(-direction.y, direction.x) / pi * 180.0f);
            ship->setVisible(!isDead);
            ship->setOpacity(isBoosting ? 128 : 255);

            massObjectSprites->setPosition(-position + centerScr);
        }
        else
        {
            beginDraw(drawNode);
            drawTriangle(drawNode, centerScr, direction, shipRadius, Color4F::WHITE);
            for (const auto& obj : massObjects)
            {
                drawCircle(drawNode, obj.pos - position + centerScr, massToRadius(obj.mass), Color4F::RED);
            }
        }
    }

    virtual bool onTouchBegan(Touch* touch, Event* event) override
    {
        if (isDead && !isEnding)
        {
            if (restartAction != nullptr)
            {
                stopAction(restartAction);
            }
            endSession();
            event->stopPropagation();
            return true;
        }

        if (isEnding)
        {
            isEnding = false;

            endDark->setVisible(false);
            endText->setVisible(false);
            resultText->setVisible(false);
            resultLabel->setVisible(false);
            bestText->setVisible(false);
            bestLabel->setVisible(false);

            startTip->setOpacity(0);
            startTip->runAction(FadeIn::create(1.5f));
            startTip->setVisible(true);

            initNewSession();

            event->stopPropagation();
            return true;
        }

        if (isOpening && nonCopyrightMode)
        {
            title->setVisible(false);
            objective->setVisible(false);
            endDark->setVisible(false);

            auto scrSize = Director::getInstance()->getWinSize();

            startTip = Sprite::create("non_copyright/startTip.png");
            startTip->setAnchorPoint(Vec2(0.5f, 0.5f));
            startTip->setPosition(Vec2(scrSize.width * 0.5f, scrSize.height * 0.25f));
            startTip->setOpacity(0);
            startTip->runAction(FadeIn::create(1.5f));
            addChild(startTip);

            isOpening = false;
            event->stopPropagation();
            return true;
        }

        if (!isLaunched)
        {
            velocity = direction.getNormalized() * startVelocity;
            isLaunched = true;

            if (startTip != nullptr) startTip->setVisible(false);

            playNonCopyrightEffect("non_copyright/launch.wav");
            if (nonCopyrightMode)
            {
                runAction(playBGMAction = Sequence::create(
                    DelayTime::create(2.0f),
                    CallFunc::create([](){
                        SimpleAudioEngine::getInstance()->playBackgroundMusic("non_copyright/backgroundMusic.mp3", true);
                    }),
                    nullptr));
            }
        }

        if (isTouchInNode(boostIcon, touch))
            tryUseSkillBoost();
        else if (isTouchInNode(bombIcon, touch))
            tryUseSkillBomb();

        if (!isInAcceleration)
        {
            isInAcceleration = true;
            event->stopPropagation();
            return true;
        }

        return false;
    }

    virtual void onTouchMoved(Touch* touch, Event* event) override
    {

    }

    virtual void onTouchEnded(Touch* touch, Event* event) override
    {
        isInAcceleration = false;
    }

    virtual void onTouchCancelled(Touch* touch, Event* event) override
    {
        isInAcceleration = false;
    }

    virtual void onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event) override
    {
        if (isDead || !isLaunched) return;

        if (keyCode == EventKeyboard::KeyCode::KEY_1)
        {
            tryUseSkillBomb();
            event->stopPropagation();
        }
        else if (keyCode == EventKeyboard::KeyCode::KEY_2)
        {
            tryUseSkillBoost();
            event->stopPropagation();
        }
    }

    void tryUseSkillBomb()
    {
        if (bombCharge <= 0) return;
            
        bombCharge--;
        if (bombCharge == 0) resetBombButton();
        else bombChargeLabel->setString(std::to_string(bombCharge));

        std::vector<MassObject>::iterator smallestIter = massObjects.begin();
        float smallestDist = std::numeric_limits<float>::max();
        for (auto iter = massObjects.begin(); iter != massObjects.end(); ++iter)
        {
            Vec2 diff = iter->pos - position;
            float dot = diff.dot(velocity.getNormalized());
            if (dot < 0) continue;

            float dist = diff.length();
            if (dist < smallestDist)
            {
                smallestDist = dist;
                smallestIter = iter;
            }
        }
        if (smallestIter != massObjects.end())
        {
            if (smallestIter->sprite != nullptr)
            {
                smallestIter->sprite->runAction(Sequence::create(
                    MoveBy::create(0.25f, (smallestIter->pos - position).getNormalized() * velocity.length()),
                    CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, smallestIter->sprite)),
                    nullptr));
            }
            massObjects.erase(smallestIter);
        }
        playNonCopyrightEffect("non_copyright/skill_bomb.wav");
    }

    void tryUseSkillBoost()
    {
        if (!boostCharge) return; 

        boostCharge = false;
        resetBoostButton();
        isBoosting = true;
        boostRemains = 1.5f;
        velocity = velocity * 3.5f;
        playNonCopyrightEffect("non_copyright/skill_boost.wav");
    }
};

bool GameScene::init()
{
    if (!Scene::init()) return false;
    
    auto layer = new GameLayer();
    layer->init();
    layer->autorelease();
    addChild(layer);

    return true;
}

GameScene* GameScene::create()
{
    auto ret = new GameScene();
    if (ret && ret->init())
        ret->autorelease();
    else
        CC_SAFE_RELEASE_NULL(ret);
    return ret;
}
